public class Henchman extends AbstractGamePiece
{
    public Henchman()
    {
        super("Henchman", "H", PLAYER_OUTLAWS);
    }
    public boolean hasEscaped()
    {
        return false;
    }
    protected boolean isSquareRestricted(GameSquare step)
    {
        if (step.getType() == GameSquare.TYPE_JAIL || step.getType() == GameSquare.TYPE_CAMP)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
