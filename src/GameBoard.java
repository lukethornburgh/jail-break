import java.util.ArrayList;
import java.util.List;

import javafx.scene.layout.GridPane;

public class GameBoard
{
    static public final int NUM_ROWS = 9;
    static public final int NUM_COLS = 9;

    private GameSquare[][] squares;

    public GameBoard(GridPane boardPane, JailBreak listener)
    {
        squares = new GameSquare[NUM_COLS][NUM_ROWS];

        for (int col = 0; col < NUM_COLS; col++)
        {
            for (int row = 0; row < NUM_ROWS; row++)
            {
                squares[col][row] = new GameSquare(col, row, boardPane, listener);
            }
        }

        squares[4][4].setType(GameSquare.TYPE_JAIL);

        squares[0][3].setType(GameSquare.TYPE_CAMP);
        squares[0][4].setType(GameSquare.TYPE_CAMP);
        squares[0][5].setType(GameSquare.TYPE_CAMP);
        squares[1][4].setType(GameSquare.TYPE_CAMP);

        squares[8][3].setType(GameSquare.TYPE_CAMP);
        squares[8][4].setType(GameSquare.TYPE_CAMP);
        squares[8][5].setType(GameSquare.TYPE_CAMP);
        squares[7][4].setType(GameSquare.TYPE_CAMP);

        squares[3][0].setType(GameSquare.TYPE_CAMP);
        squares[4][0].setType(GameSquare.TYPE_CAMP);
        squares[5][0].setType(GameSquare.TYPE_CAMP);
        squares[4][1].setType(GameSquare.TYPE_CAMP);

        squares[3][8].setType(GameSquare.TYPE_CAMP);
        squares[4][8].setType(GameSquare.TYPE_CAMP);
        squares[5][8].setType(GameSquare.TYPE_CAMP);
        squares[4][7].setType(GameSquare.TYPE_CAMP);
    }

    public void reset()
    {
        for (int col = 0; col < NUM_COLS; col++)
        {
            for (int row = 0; row < NUM_ROWS; row++)
            {
                squares[col][row].clearSquare();
                squares[col][row].deselect();
            }
        }
    }

    public void setPiece(int col, int row, AbstractGamePiece piece)
    {
        if ((col >= 0) && (col < NUM_COLS) && (row >= 0) && (row < NUM_ROWS))
        {
            squares[col][row].setPiece(piece);
        }
    }

    AbstractGamePiece getPiece(int col, int row)
    {
        if ((col >= 0) && (col < NUM_COLS) && (row >= 0) && (row < NUM_ROWS))
        {
            return squares[col][row].getPiece();
        }

        return null;
    }

    public void removePiece(AbstractGamePiece piece)
    {
        int col = piece.getCol();
        int row = piece.getRow();

        GameSquare square = squares[col][row];

        square.clearSquare();
    }

    public List<GameSquare> buildPath(GameSquare startingSquare, GameSquare targetSquare)
    {
        List<GameSquare> path = new ArrayList<GameSquare>();

        int currentCol = startingSquare.getCol();
        int currentRow = startingSquare.getRow();

        int endCol = targetSquare.getCol();
        int endRow = targetSquare.getRow();

        if ((currentCol == endCol) && (currentRow == endRow))
        {
            return path;
        }

        if ((currentCol != endCol) && (currentRow != endRow))
        {
            return path;
        }

        if ((currentCol == endCol) && (currentRow < endRow))
        {
            while (currentRow < endRow)
            {
                currentRow++;
                path.add(squares[currentCol][currentRow]);
            }
        }
        else if ((currentCol == endCol) && (currentRow > endRow))
        {
            while (currentRow > endRow)
            {
                currentRow--;
                path.add(squares[currentCol][currentRow]);
            }
        }
        else if ((currentCol > endCol) && (currentRow == endRow))
        {
            while (currentCol > endCol)
            {
                currentCol--;
                path.add(squares[currentCol][currentRow]);
            }
        }
        else if ((currentCol < endCol) && (currentRow == endRow))
        {
            while (currentCol < endCol)
            {
                currentCol++;
                path.add(squares[currentCol][currentRow]);
            }
        }

        return path;
    }
}
