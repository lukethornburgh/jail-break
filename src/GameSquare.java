import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class GameSquare
{
    static public final int TYPE_NEUTRAL = 0;
    static public final int TYPE_JAIL = 1;
    static public final int TYPE_CAMP = 2;

    private int myType;
    private int myCol;
    private int myRow;

    private Button myButton;

    Color myColor;

    private AbstractGamePiece myPiece;

    static Border selectedBorder = new Border(new BorderStroke(Color.YELLOW, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(3)));
    static Border unselectedBorder = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(1)));

    public GameSquare(int col, int row, GridPane pane, JailBreak listener)
    {
        myType = TYPE_NEUTRAL;
        myCol = col;
        myRow = row;

        myButton = new Button();
        myButton.setPrefSize(50, 50);
        myColor = Color.WHITE;
        myButton.setBackground(new Background(new BackgroundFill(myColor, CornerRadii.EMPTY, Insets.EMPTY)));
        myButton.setBorder(unselectedBorder);

        myButton.setOnAction((ActionEvent event) -> listener.squareClick(this));

        pane.add(myButton, myCol, myRow);
    }

    public void setType(int type)
    {
        myType = type;
        if (myType == TYPE_NEUTRAL)
        {
            myColor = Color.WHITE;
        }
        else if (myType == TYPE_JAIL)
        {
            myColor = Color.CYAN;
        }
        else
        {
            myColor = Color.rgb(220, 220, 220);
        }

        myButton.setBackground(new Background(new BackgroundFill(myColor, CornerRadii.EMPTY, Insets.EMPTY)));
    }

    public int getType()
    {
        return myType;
    }

    public AbstractGamePiece getPiece()
    {
        return myPiece;
    }

    public int getCol()
    {
        return myCol;
    }

    public int getRow()
    {
        return myRow;
    }

    public void setPiece(AbstractGamePiece piece)
    {
        myPiece = piece;

        piece.setPosition(myCol, myRow);

        if(piece.getPlayerType() == AbstractGamePiece.PLAYER_OUTLAWS)
        {
            myButton.setFont(new Font("System Bold", 16));
        }
        else
        {
            myButton.setFont(new Font("System Italic", 18));
        }

        myButton.setText(myPiece.getAbbreviation());
    }

    public void clearSquare()
    {
        myPiece = null;
        myButton.setText("");
    }

    public void select()
    {
        myButton.setBorder(selectedBorder);
    }

    public void deselect()
    {
        myButton.setBorder(unselectedBorder);
    }
}
