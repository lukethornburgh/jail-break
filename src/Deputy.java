public class Deputy extends AbstractGamePiece
{
    public Deputy()
    {
        super("Deputy", "D", PLAYER_POSSE);
    }
    public boolean hasEscaped()
    {
        return false;
    }
    protected boolean isSquareRestricted(GameSquare step)
    {
        if (step.getType() == GameSquare.TYPE_JAIL)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
