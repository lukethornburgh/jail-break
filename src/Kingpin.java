public class Kingpin extends AbstractGamePiece
{
    public Kingpin()
    {
        super("Kingpin", "K", PLAYER_OUTLAWS);
    }
    public boolean hasEscaped()
    {
        if (myCol == 0 || myCol == GameBoard.NUM_COLS - 1 ||myRow == 0 || myRow == GameBoard.NUM_ROWS - 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    protected boolean isSquareRestricted(GameSquare step)
    {
        if (step.getType() == GameSquare.TYPE_JAIL || step.getType() == GameSquare.TYPE_CAMP)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    @Override
    public boolean isCaptured(GameBoard gameBoard)
    {
        AbstractGamePiece nearbyPiece1 = gameBoard.getPiece(myCol, myRow - 1);
        AbstractGamePiece nearbyPiece2 = gameBoard.getPiece(myCol, myRow + 1);
        AbstractGamePiece nearbyPiece3 = gameBoard.getPiece(myCol - 1, myRow);
        AbstractGamePiece nearbyPiece4 = gameBoard.getPiece(myCol + 1, myRow);
        if ((nearbyPiece1 != null) && (nearbyPiece1.getPlayerType() != myPlayerType) && (nearbyPiece2 != null) && (nearbyPiece2.getPlayerType() != myPlayerType) && (nearbyPiece3 != null) && (nearbyPiece3.getPlayerType() != myPlayerType) && (nearbyPiece4 != null) && (nearbyPiece4.getPlayerType() != myPlayerType))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
