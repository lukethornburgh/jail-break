import java.util.ArrayList;
import java.util.List;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class JailBreak extends Application
{
    public static void main(String[] args)
    {
        launch(args);
    }

    private Label playerTurnLabel;

    private GameBoard gameBoard = null;

    private GameSquare selectedSquare = null;

    private boolean gameOver = false;

    private int currentPlayerTurn;

    private AbstractGamePiece kingpinPiece = null;

    @Override
    public void start(Stage primaryStage)
    {
        primaryStage.setTitle("Jail Break");
        primaryStage.setResizable(false);

        VBox root = new VBox(5);
        root.setPadding(new Insets(5));
        root.setAlignment(Pos.CENTER);
        primaryStage.setScene(new Scene(root));

        playerTurnLabel = new Label();
        root.getChildren().add(playerTurnLabel);

        GridPane boardPane = new GridPane();
        gameBoard = new GameBoard(boardPane, this);
        root.getChildren().add(boardPane);

        Button resetButton = new Button("Reset");
        resetButton.setOnAction((ActionEvent event) -> reset());
        root.getChildren().add(resetButton);

        reset();

        primaryStage.sizeToScene();
        primaryStage.show();
    }

    private void reset()
    {
        gameOver = false;
        gameBoard.reset();
        kingpinPiece = new Kingpin();
        Henchman henchmanPiece1 = new Henchman();
        Henchman henchmanPiece2 = new Henchman();
        Henchman henchmanPiece3 = new Henchman();
        Henchman henchmanPiece4 = new Henchman();
        Henchman henchmanPiece5 = new Henchman();
        Henchman henchmanPiece6 = new Henchman();
        Henchman henchmanPiece7 = new Henchman();
        Henchman henchmanPiece8 = new Henchman();
        Deputy deputyPiece01 = new Deputy();
        Deputy deputyPiece02 = new Deputy();
        Deputy deputyPiece03 = new Deputy();
        Deputy deputyPiece04 = new Deputy();
        Deputy deputyPiece05 = new Deputy();
        Deputy deputyPiece06 = new Deputy();
        Deputy deputyPiece07 = new Deputy();
        Deputy deputyPiece08 = new Deputy();
        Deputy deputyPiece09 = new Deputy();
        Deputy deputyPiece10 = new Deputy();
        Deputy deputyPiece11 = new Deputy();
        Deputy deputyPiece12 = new Deputy();
        Deputy deputyPiece13 = new Deputy();
        Deputy deputyPiece14 = new Deputy();
        Deputy deputyPiece15 = new Deputy();
        Deputy deputyPiece16 = new Deputy();
        gameBoard.setPiece(4, 4, kingpinPiece);
        gameBoard.setPiece(4, 3, henchmanPiece1);
        gameBoard.setPiece(4, 2, henchmanPiece2);
        gameBoard.setPiece(4, 5, henchmanPiece3);
        gameBoard.setPiece(4, 6, henchmanPiece4);
        gameBoard.setPiece(3, 4, henchmanPiece5);
        gameBoard.setPiece(2, 4, henchmanPiece6);
        gameBoard.setPiece(5, 4, henchmanPiece7);
        gameBoard.setPiece(6, 4, henchmanPiece8);
        gameBoard.setPiece(0, 3, deputyPiece01);
        gameBoard.setPiece(0, 4, deputyPiece02);
        gameBoard.setPiece(0, 5, deputyPiece03);
        gameBoard.setPiece(1, 4, deputyPiece04);
        gameBoard.setPiece(3, 8, deputyPiece05);
        gameBoard.setPiece(4, 8, deputyPiece06);
        gameBoard.setPiece(5, 8, deputyPiece07);
        gameBoard.setPiece(4, 7, deputyPiece08);
        gameBoard.setPiece(8, 3, deputyPiece09);
        gameBoard.setPiece(8, 4, deputyPiece10);
        gameBoard.setPiece(8, 5, deputyPiece11);
        gameBoard.setPiece(7, 4, deputyPiece12);
        gameBoard.setPiece(3, 0, deputyPiece13);
        gameBoard.setPiece(4, 0, deputyPiece14);
        gameBoard.setPiece(5, 0, deputyPiece15);
        gameBoard.setPiece(4, 1, deputyPiece16);
        currentPlayerTurn = AbstractGamePiece.PLAYER_OUTLAWS;
        setPlayerTurnLabel();
    }

    private void changePlayerTurn()
    {
        if(currentPlayerTurn == AbstractGamePiece.PLAYER_OUTLAWS)
        {
            currentPlayerTurn = AbstractGamePiece.PLAYER_POSSE;
        }
        else
        {
            currentPlayerTurn = AbstractGamePiece.PLAYER_OUTLAWS;
        }
    }

    private void setPlayerTurnLabel()
    {
        if(currentPlayerTurn == AbstractGamePiece.PLAYER_OUTLAWS)
        {
            playerTurnLabel.setText("Player Turn: Outlaws!");
        }
        else
        {
            playerTurnLabel.setText("Player Turn: Posse!");
        }
    }

    public void squareClick(GameSquare clickedSquare)
    {
        if(!gameOver)
        {
            if (clickedSquare != null)
            {
                handleClickedSquare(clickedSquare);

                setPlayerTurnLabel();
            }
        }
    }

    private void handleClickedSquare(GameSquare clickedSquare)
    {
        if (selectedSquare == null)
        {
            if (clickedSquare.getPiece() != null)
            {
                if (clickedSquare.getPiece().getPlayerType() == currentPlayerTurn)
                {
                    selectedSquare = clickedSquare;
                    selectedSquare.select();
                }
            }
        }
        else if (selectedSquare == clickedSquare)
        {
            selectedSquare.deselect();
            selectedSquare = null;
        }
        else
        {
            AbstractGamePiece selectedPiece = selectedSquare.getPiece();
            List<GameSquare> path = gameBoard.buildPath(selectedSquare, clickedSquare);
            boolean validMove = selectedPiece.canMoveToLocation(path);
            if (validMove)
            {
                clickedSquare.setPiece(selectedPiece);
                selectedSquare.clearSquare();
                selectedSquare.deselect();
                selectedSquare = null;
                boolean kingpinCaptured = false;
                for (AbstractGamePiece capturedOpponent : findCapturedOpponents(selectedPiece.getCol(), selectedPiece.getRow()))
                {
                    showMessageDialog(capturedOpponent.toString() + " was captured!");
                    gameBoard.removePiece(capturedOpponent);
                    if (capturedOpponent == kingpinPiece)
                    {
                        kingpinCaptured = true;
                    }
                }
                if (kingpinCaptured)
                {
                    gameOver = true;
                    showMessageDialog("Game Over: Kingpin captured");
                }
                else if (kingpinPiece.hasEscaped())
                {
                    gameOver = true;
                    showMessageDialog("Game Over: Kingpin escaped");
                }
                else
                {
                    changePlayerTurn();
                }
            }
        }
    }

    private List<AbstractGamePiece> findCapturedOpponents(int col, int row)
    {
        ArrayList<AbstractGamePiece> capturedOpponents = new ArrayList<AbstractGamePiece>();

        AbstractGamePiece nearbyPiece = null;

        nearbyPiece = gameBoard.getPiece(col, row - 1);
        if ((nearbyPiece != null) && (nearbyPiece.isCaptured(gameBoard)))
        {
            capturedOpponents.add(nearbyPiece);
        }

        nearbyPiece = gameBoard.getPiece(col, row + 1);
        if ((nearbyPiece != null) && (nearbyPiece.isCaptured(gameBoard)))
        {
            capturedOpponents.add(nearbyPiece);
        }

        nearbyPiece = gameBoard.getPiece(col - 1, row);
        if ((nearbyPiece != null) && (nearbyPiece.isCaptured(gameBoard)))
        {
            capturedOpponents.add(nearbyPiece);
        }

        nearbyPiece = gameBoard.getPiece(col + 1, row);
        if ((nearbyPiece != null) && (nearbyPiece.isCaptured(gameBoard)))
        {
            capturedOpponents.add(nearbyPiece);
        }

        return capturedOpponents;
    }

    public static void showMessageDialog(String message)
    {
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Message");
        dialogStage.setWidth(300);

        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.initStyle(StageStyle.UTILITY);
        dialogStage.setResizable(false);

        VBox dialogRoot = new VBox(5);
        dialogRoot.setPadding(new Insets(10));
        dialogRoot.setAlignment(Pos.CENTER);
        dialogStage.setScene(new Scene(dialogRoot));

        Label dialogLabel = new Label(message);

        Button dialogButton = new Button("OK");
        dialogButton.setOnAction((ActionEvent dialogEvent) -> dialogStage.close());

        dialogRoot.getChildren().add(dialogLabel);
        dialogRoot.getChildren().add(dialogButton);

        dialogStage.show();
    }
}
